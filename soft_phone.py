from google.appengine.api import channel
from google.appengine.api import users
from google.appengine.ext import ndb

from twilio.rest import TwilioRestClient
from twilio.util import TwilioCapability
from twilio import twiml

import webapp2
import json
import os, jinja2
import logging

import secret

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True
)

account_sid = secret.twilio_account_sid
auth_token  = secret.twilio_auth_token
azalea_app = 'APbc14607da47151336b8877c44a993599'

# Data

# TODO keep track of the active sessions, have an interface to list them.

class ClientSession(ndb.Model):
    user_id = ndb.StringProperty()
    email = ndb.StringProperty()
    active = ndb.BooleanProperty()

# Web

class PlaySpace(webapp2.RequestHandler):
    def get(self):
        self.response.headers['content-type'] = 'text/plain'
        out = self.response.out

        pass

class SoftPhone(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        user_id = user.user_id()
        user_email = user.email()

        capability = TwilioCapability(account_sid, auth_token)
        capability.allow_client_incoming(user_id)
        capability.allow_client_outgoing(azalea_app)
        token = capability.generate()

        session = ClientSession(
            user_id=user_id,
            email=user_email,
            active=True,
        )
        session_key = session.put()

        channel_token = channel.create_channel(session_key.urlsafe());

        template_values = {
            'token': token,
            'channel_token': channel_token,
            'session_key': session_key,
        }
        template = jinja_environment.get_template('public/soft-phone.html')
        self.response.write(template.render(template_values))

# Message

class MessageMain(webapp2.RequestHandler):
    def post(self):
        smsBody = self.request.get('Body');
        smsFrom = self.request.get('From');
        message = {
            'from': smsFrom,
            'body': smsBody
        }
        message = json.dumps(message)
        q = ClientSession.query(
            ClientSession.email == 'sarah.a.happy@gmail.com',
            ClientSession.active == True
        )
        for session in q:
            channel_token = session.key.urlsafe()
            logging.info('sending %s %s' % (channel_token, message))
            channel.send_message(channel_token, message)

        script = twiml.Response()
        return xml_response(str(script))

    get = post

class ChannelConnected(webapp2.RequestHandler):
    def post(self):
        client_id = self.request.get('from')

class ChannelDisconnected(webapp2.RequestHandler):
    def post(self):
        client_id = self.request.get('from')
        session = ndb.Key(urlsafe=client_id).get()
        session.active = False
        session.put()

# Voice

class VoiceMain(webapp2.RequestHandler):
    def post(self):
        action = self.request.get('action', 'main')
        source = self.request.get('From', None)

        if action == 'main':
            script = twiml.Response()
            gather = script.gather(
                action="voice_main?action=pin",
                timeout=5,
                finishOnKey='#'
            )
            gather.say('Please dial 22 for Live Painter Sharon Epic.')
            gather.say('dial 11 for Dish Fairy Sarah Happy.')
            gather.say('dial 13 for I can do that errands.')
            gather.say('dial 33 for Story time soul cards.')
            script.dial().client('fox')
            return xml_response(str(script))

        if action == 'pin':
            digits = self.request.get('Digits')

            if digits == '22':
                # dial 22 for Live Painter Sharon Epic.
                script = twiml.Response()
                script.dial().number('+19027191322')
                return xml_response(str(script))

            if digits == '11':
                # dial 11 for Dish Fairy Sarah Happy.
                script = twiml.Response()
                dial = script.dial()
                dial.number('+15062925251')
                dial.client('101457948775771973107')
                return xml_response(str(script))

            if digits == '13':
                # dial 13 for I can do that errands.
                script = twiml.Response()
                script.dial().number('+15064406513')
                return xml_response(str(script))

            if digits == '33':
                # dial 33 for Story time soul cards.
                script = twiml.Response()
                script.dial().number('+15062608884')
                return xml_response(str(script))

            if digits == '4367':
                # calling card
                script = twiml.Response()
                gather = script.gather(
                    action="voice_main?action=call",
                    timeout=10,
                    finishOnKey='#')
                gather.say('Please enter a phone number and then press pound.')
                return xml_response(str(script))

            # anything else
            script = twiml.Response()
            script.say('I do not know what do now. Thank you for calling.')
            return xml_response(str(script))

            # script = twiml.Response()
            # script.gather(action="voice_main?action=pin", timeout=10, finishOnKey='#').say('That code did not work, please enter your pin number and then press pound.')
            # return xml_response(str(script))

        if action == 'call':
            digits = self.request.get('Digits')
            script = twiml.Response()
            script.dial().number(digits)
            return xml_response(str(script))

        script = twiml.Response()
        script.dial().client('fox')
        return xml_response(str(script))

    get = post

class DialOut(webapp2.RequestHandler):
    def post(self):
        from_happy = False
        client = self.request.get('From')
        logging.info('From = %s' % client)

        number = self.request.get('number')

        script = twiml.Response()
        if client == 'client:101457948775771973107':
            script.dial(callerId='+15062925251').number(number)
        else:
            script.dial(callerId='+18555710222').number(number)
        return xml_response(str(script))

    get = post

def xml_response(body):
    return webapp2.Response(str(body), content_type='text/xml')

# Wire

routes = [
    ('/soft_phone', SoftPhone),
    ('/voice_main', VoiceMain),
    ('/dial_out', DialOut),
    ('/message_main', MessageMain),
    ('/play_space', PlaySpace),
    ('/_ah/channel/connected/', ChannelConnected),
    ('/_ah/channel/disconnected/', ChannelDisconnected),
]

app = webapp2.WSGIApplication(routes, debug=True)
