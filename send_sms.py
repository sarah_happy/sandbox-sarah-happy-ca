from twilio.rest import TwilioRestClient

import webapp2
import os, jinja2
import secret

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

account_sid = secret.twilio_account_sid
auth_token  = secret.twilio_auth_token

class SendSms(webapp2.RequestHandler):
    def post(self):
        client = TwilioRestClient(account_sid, auth_token)

        message = client.messages.create(body="I love you Happy <3",
                to="+15062925251",    # Replace with your phone number
                from_="+17786546531") # Replace with your Twilio number

        template_values = {
                'message': message,
                }
        template = JINJA_ENVIRONMENT.get_template('public/send-sms-done.html')
        self.response.write(template.render(template_values))

app = webapp2.WSGIApplication([('/send_sms', SendSms)], debug=True)
